package com.permissionservice.controller;
import com.permissionservice.exception.PermissionException;
import com.permissionservice.model.Permission;
import com.permissionservice.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import request.ObjectRequest;
import response.ObjectResponse;

import javax.validation.ConstraintViolationException;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/permissions")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@PreAuthorize("hasAuthority('ADMIN')")
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    @PostMapping("/filter")
    public ResponseEntity<ObjectResponse> getPermissionByFilter(@RequestBody ObjectRequest objectRequest){
        try{
            Page<Permission> page = permissionService.getPermissionByFilter(objectRequest);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get user successfully",page));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Create new permission*/
    @PostMapping
    public ResponseEntity<ObjectResponse> createPermission(@RequestBody Permission permission){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create permission successfully",permissionService.addPermission(permission)));
        }
        catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (PermissionException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Update permission*/
    @PutMapping("/{id}")
    public ResponseEntity<ObjectResponse> updatePermission(@PathVariable("id") String id, @RequestBody Permission permission, Principal principal){
        try {
            System.err.println(principal.toString());
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Update permission successfully",permissionService.updatePermission(id,permission)));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (PermissionException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Delete data permission*/
    @DeleteMapping("/{id}")
    public ResponseEntity<ObjectResponse> deletePermission(@PathVariable("id") String id){
        try {
            permissionService.deletePermission(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Delete permission with id: " +id+ " successfully",""));
        }catch (PermissionException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }


}
