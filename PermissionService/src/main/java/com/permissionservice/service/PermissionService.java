package com.permissionservice.service;

import com.permissionservice.exception.PermissionException;
import com.permissionservice.model.Permission;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import request.ObjectRequest;

import javax.validation.ConstraintViolationException;

public interface PermissionService {

    @Transactional
    Permission addPermission(Permission permission) throws PermissionException, ConstraintViolationException;

    @Transactional
    Permission updatePermission(String id, Permission permission) throws PermissionException;

    @Transactional
    void deletePermission(String id) throws PermissionException;
    @Transactional
    Page<Permission> getPermissionByFilter(ObjectRequest objectRequest) throws Exception;
}
