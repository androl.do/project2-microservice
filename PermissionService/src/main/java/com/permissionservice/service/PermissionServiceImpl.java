package com.permissionservice.service;

import com.permissionservice.exception.PermissionException;
import com.permissionservice.model.Permission;
import com.permissionservice.respository.PermissionCustomRepository;
import com.permissionservice.respository.PermissionRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import request.ObjectRequest;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

@Service
public class PermissionServiceImpl implements PermissionService{
    @Autowired
    private PermissionRepository permissionRepo;

    @Autowired
    private PermissionCustomRepository permissionCustomRepo;

    @Override
    @Transactional
    public Permission addPermission(Permission permission) throws PermissionException, ConstraintViolationException {
       if ( permissionRepo.findByCode(permission.getCode()).isPresent()){
           throw new PermissionException(PermissionException.permissionCodeAlreadyExist());
       }
       return permissionRepo.save(permission);
    }

    @Override
    @Transactional
    public Permission updatePermission(String id, Permission permission) throws PermissionException {
        Optional<Permission> permissionOpt = permissionRepo.findById(id);
        permissionOpt.orElseThrow(()->
            new PermissionException(PermissionException.notFoundException(id))
        );

        if ( permissionRepo.findByCode(permission.getCode()).isPresent()){
            throw new PermissionException(PermissionException.permissionCodeAlreadyExist());
        }

        Permission oldPermission = permissionOpt.get();
        oldPermission.setCode(StringUtils.isBlank(permission.getCode())? oldPermission.getCode() : permission.getCode());
        oldPermission.setName(StringUtils.isBlank(permission.getName())? oldPermission.getName() : permission.getName());
        oldPermission.setRule(StringUtils.isBlank(permission.getRule())? oldPermission.getRule() : permission.getRule());

        return permissionRepo.save(oldPermission);
    }

    @Override
    @Transactional
    public void deletePermission(String id) throws PermissionException {
        Optional<Permission> permissionOpt = permissionRepo.findById(id);
        if (!permissionOpt.isPresent()){
            throw new PermissionException(PermissionException.notFoundException(id));
        }
        permissionRepo.delete(permissionOpt.get());
    }

    @Override
    @Transactional
    public Page<Permission> getPermissionByFilter(ObjectRequest objectRequest) throws Exception {
        return permissionCustomRepo.findUsersByProperties(objectRequest);
    }



}
