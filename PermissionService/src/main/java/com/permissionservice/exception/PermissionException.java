package com.permissionservice.exception;

public class PermissionException extends  Exception{

    public PermissionException(String message) {
        super(message);
    }

    public  static String notFoundException(String id){
        return "Permission with id: " + id + " not found";
    }

    public  static String permissionCodeAlreadyExist(){
        return "This permission code already exist";
    }

}
