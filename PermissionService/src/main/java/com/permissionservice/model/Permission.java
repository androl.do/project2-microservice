package com.permissionservice.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Document("permissions")
@Data
public class Permission {
    @Id
    private String id;

    @NotBlank(message = "Permission code is mandatory")
    private String code;

    @NotBlank(message = "Permission name is mandatory")
    private String name;

    @NotBlank(message = "Rule is mandatory")
    private String rule;
}
