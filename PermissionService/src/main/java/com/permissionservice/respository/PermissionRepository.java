package com.permissionservice.respository;

import com.permissionservice.model.Permission;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PermissionRepository extends MongoRepository<Permission,String> {
    Optional<Permission> findByCode(String code);
}
