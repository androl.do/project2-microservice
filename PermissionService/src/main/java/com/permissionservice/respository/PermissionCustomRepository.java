package com.permissionservice.respository;
import com.permissionservice.model.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;
import request.ObjectRequest;

@Repository
public class PermissionCustomRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public Page<Permission> findUsersByProperties(ObjectRequest objectRequest) throws Exception {
        try {
            return PageableExecutionUtils.getPage(mongoTemplate.find(objectRequest.toQueryMongo(),Permission.class), objectRequest.getPageable(),
                    ()-> mongoTemplate.count(objectRequest.toQueryMongo(), Permission.class));
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Search param invalid",e);
        }
    }
}
