package com.userservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Set;

@Data
@Document(collection = "users")
public class User {
    @Id
    private String id;

    @NotBlank(message = "Username is mandatory")
    @Indexed(unique = true)
    private String username;

    @NotBlank(message = "First name is mandatory")
    @TextIndexed(weight = 9)
    private String firstName;

    @NotBlank(message = "Last name is mandatory")
    @TextIndexed(weight = 10)
    private String lastName;

    @Email(message = "Email invalid")
    @Indexed(unique = true)
    private String email;

    @Indexed(unique = true)
    @Pattern(regexp = "^(0|\\+84)(\\s|\\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\\d)(\\s|\\.)?(\\d{3})(\\s|\\.)?(\\d{3})$",message = "Phone invalid")
    private String phone;

    @NotNull(message = "Address can't be null")
    private Address address;

    private Boolean status;

    @NotBlank(message = "Password is mandatory")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonIgnore
    private String tokenResetPassword;

    @NotNull(message = "Rule is mandatory")
    private List<Rule> rules;
}
