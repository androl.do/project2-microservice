package com.userservice.controller;

import com.userservice.dto.PasswordDto;
import com.userservice.exception.UserException;
import com.userservice.model.ObjectResponse;
import com.userservice.model.User;
import com.userservice.service.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import request.ObjectRequest;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.ConstraintViolationException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JavaMailSender mailSender;

    /*Get users , filter and sort*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/filter")
    public ResponseEntity<ObjectResponse> getUsersByFilter(@RequestBody ObjectRequest objectRequest){
        try{
            Page<User> page = userService.getUsersByFilterAndSort(objectRequest);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get user successfully",page));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Create new user*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping()
    public ResponseEntity<ObjectResponse> createUser(@RequestBody User user){
        try {
            user =  userService.createUser(user);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create user successfully",user));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (UserException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Update infor user*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ObjectResponse> updateUser(@PathVariable("id") String id,@RequestBody User user){
        try {
            user =  userService.updateUser(id,user);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Update user successfully",user));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (UserException e){
            return ResponseEntity.status(e.equals(UserException.notFoundException(id))?HttpStatus.NOT_FOUND:HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Delete user not remove data, change status to false*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ObjectResponse> deleteUser(@PathVariable("id") String id){
        try {
            userService.deleteUser(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Delete user with id: " + id + " successfully",""));
        }catch (UserException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Search user by text search first,last name*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/search")
    public ResponseEntity<ObjectResponse> searchUserByName(@RequestParam("key") String key){
        List<User> users = userService.searchUserByName(key);
        if (users.size()>0){
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get customers name successfully",users));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found user with name: " + key,users));
        }
    }

    /*Change password user*/
    @PutMapping("/change-password/{id}")
    public ResponseEntity<ObjectResponse> changePassword(@PathVariable("id") String id, @RequestBody PasswordDto passwordDto){
        try {
            userService.changePassword(id,passwordDto);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Change password successfully",""));
        }catch (UserException e){
            return ResponseEntity.status(e.equals(UserException.notFoundException(id)) ? HttpStatus.NOT_FOUND:HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Forget password - send token to email and save token*/
    @GetMapping("/forget-password")
    public ResponseEntity<ObjectResponse> forgetPassword(@RequestParam("email") String email){
        Optional<User> userOpt = userService.getUserByEmail(email);
        if (userOpt.isPresent()){
            String token = RandomString.make(30);
            try {
                sendEmail(email,token);
                User user = userOpt.get();
                user.setTokenResetPassword(token);
                userService.updateUser(user);
                return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Token has been sent to the mail",""));
            } catch (MessagingException | UnsupportedEncodingException e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ObjectResponse(ObjectResponse.FAILED,"Send mail error",""));
            }
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not fount email: " + email,""));
        }
    }

    /*Reset password - check token, if true update new password for user*/
    @PutMapping("/reset-password")
    public ResponseEntity<ObjectResponse> resetPassword(@RequestParam("email") String email, @RequestBody PasswordDto passwordDto){
        Optional<User> userOpt = userService.getUserByEmail(email);
        if (userOpt.isPresent()){
            User user = userOpt.get();
            if (passwordDto.getToken().length()>0 && user.getTokenResetPassword().equalsIgnoreCase(passwordDto.getToken())){
                userService.resetPassword(user);
                return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Reset password successfully",""));
            }
            else{
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,"Token incorrect",""));
            }
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found email: " + email,""));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ObjectResponse> getUserById(@PathVariable("id") String id){
        Optional<User> userrOpt = userService.getUserById(id);
        if (userrOpt.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get user by code successfully",userrOpt.get()));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found user with id: " + id,""));
        }
    }

    /*Send token to email of user*/
    public void sendEmail(String recipientEmail, String token)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("contact@trungngoc.com", "TrungNgoc Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the token to reset your password";

        String content = "Token: " + token;

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }

}
