package com.userservice.repository;
import com.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;
import request.ObjectRequest;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Page<User> findUsersByProperties(ObjectRequest objectRequest) throws Exception {
        try {
            return PageableExecutionUtils.getPage(mongoTemplate.find(objectRequest.toQueryMongo(),User.class), objectRequest.getPageable(),
                    ()-> mongoTemplate.count(objectRequest.toQueryMongo(), User.class));
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Search param invalid",e);
        }
    }
}
