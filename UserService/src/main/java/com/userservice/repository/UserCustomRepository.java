package com.userservice.repository;
import com.userservice.model.User;
import org.springframework.data.domain.Page;
import request.ObjectRequest;


public interface UserCustomRepository {
    Page<User> findUsersByProperties(ObjectRequest objectRequest) throws Exception;
}
