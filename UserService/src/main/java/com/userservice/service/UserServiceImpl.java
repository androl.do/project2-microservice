package com.userservice.service;

import com.userservice.dto.PasswordDto;
import com.userservice.exception.UserException;
import com.userservice.model.User;
import com.userservice.repository.UserCustomRepository;
import com.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import request.ObjectRequest;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEn;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UserCustomRepository userCustomRepo;

    @Override
    public User createUser(User newUser) throws UserException, ConstraintViolationException {
        if (userRepo.findUserByUsername(newUser.getUsername()).isPresent()) {
            throw new UserException(UserException.usernameAlreadyExist());
        }
        if (userRepo.findUserByEmail(newUser.getEmail()).isPresent()) {
            throw new UserException(UserException.emailAlreadyExist());
        }
        if (userRepo.findUserByPhone(newUser.getPhone()).isPresent()) {
            throw new UserException(UserException.phoneAlreadyExist());
        }

        newUser.setPassword(bCryptPasswordEn.encode(newUser.getPassword()));
        newUser.setStatus(true);
        return userRepo.save(newUser);

    }


    @Override
    public User updateUser(String id, User user) throws UserException,ConstraintViolationException {
        Optional<User> useOp = userRepo.findById(id);

        if (!useOp.isPresent()) {
            throw new UserException(UserException.notFoundException(id));
        }

        User oldUser = useOp.get();
        if (user.getUsername() != null) {
            if (userRepo.findUserByUsername(user.getUsername()).isPresent()) {
                throw new UserException(UserException.usernameAlreadyExist());
            } else {
                oldUser.setUsername(user.getUsername());
            }
        }
        if (user.getEmail() != null) {
            if (userRepo.findUserByEmail(user.getEmail()).isPresent()) {
                throw new UserException(UserException.emailAlreadyExist());
            } else {
                oldUser.setEmail(user.getEmail());
            }
        }
        if (user.getPhone() != null) {
            if (userRepo.findUserByPhone(user.getPhone()).isPresent()) {
                throw new UserException(UserException.phoneAlreadyExist());
            } else {
                oldUser.setPhone(user.getPhone());
            }
        }
        oldUser.setFirstName(user.getFirstName() == null ? oldUser.getFirstName() : user.getFirstName());
        oldUser.setLastName(user.getLastName() == null ? oldUser.getLastName() : user.getLastName());
        oldUser.setStatus(user.getStatus() == null ? oldUser.getStatus() : user.getStatus());
        oldUser.setAddress(user.getAddress() == null ? oldUser.getAddress() : user.getAddress());
        return userRepo.save(oldUser);

    }

    @Override
    public void deleteUser(String id) throws UserException {
        Optional<User> userOp = userRepo.findById(id);
        if (userOp.isPresent()) {
            User oldUser = userOp.get();
            oldUser.setStatus(false);
            userRepo.save(oldUser);
        } else {
            throw new UserException(UserException.notFoundException(id));
        }
    }

    /* search user by key in first name or last name*/
    @Override
    public List<User> searchUserByName(String key) {
        List<User> users = userRepo.searchByName(key);
        if (!users.isEmpty()) {
            return users;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void changePassword(String id, PasswordDto passwordDto) throws UserException {
        Optional<User> userOp = userRepo.findById(id);
        if (userOp.isPresent()) {
            User oldUser = userOp.get();
            if (bCryptPasswordEn.matches(passwordDto.getOldPassword(), oldUser.getPassword())) {
                oldUser.setPassword(bCryptPasswordEn.encode(passwordDto.getNewPassword()));
            } else {
                throw new UserException("The old password incorrect");
            }
        } else {
            throw new UserException(UserException.notFoundException(id));
        }
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepo.findUserByEmail(email);
    }

    @Override
    public void updateUser(User user) {
        userRepo.save(user);
    }

    @Override
    public void resetPassword(User user) {
        user.setPassword(bCryptPasswordEn.encode(user.getPassword()));
        user.setTokenResetPassword("");
        userRepo.save(user);
    }

    @Override
    public Page<User> getUsersByFilterAndSort(ObjectRequest objectRequest) throws Exception {
        return userCustomRepo.findUsersByProperties(objectRequest);
    }

    @Override
    public Optional<User> getUserById(String id){
        return userRepo.findUserById(id);
    }


}
