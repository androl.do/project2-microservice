package com.userservice.service;

import com.userservice.dto.PasswordDto;
import com.userservice.exception.UserException;
import com.userservice.model.User;
import org.springframework.data.domain.Page;
import request.ObjectRequest;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    User createUser(User newUser) throws UserException, ConstraintViolationException;

    User updateUser(String id, User user) throws UserException,ConstraintViolationException;

    void deleteUser(String id) throws UserException;

    List<User> searchUserByName(String key);

    void changePassword(String id, PasswordDto passwordDto) throws UserException;

    Optional<User> getUserByEmail(String email);

    void updateUser(User user);

    void resetPassword(User user);

    Page<User> getUsersByFilterAndSort(ObjectRequest objectRequest) throws Exception;

    Optional<User> getUserById(String id);
}
