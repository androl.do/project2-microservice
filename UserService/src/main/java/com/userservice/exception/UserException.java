package com.userservice.exception;

public class UserException extends  Exception{

    public UserException(String message) {
        super(message);
    }

    public  static String notFoundException(String id){
        return "User with id: " + id + " not found";
    }

    public  static String emailAlreadyExist(){
        return "This email already exist";
    }

    public  static String usernameAlreadyExist(){
        return "This username already exist";
    }

    public  static String phoneAlreadyExist(){
        return "This phone already exist";
    }

}
