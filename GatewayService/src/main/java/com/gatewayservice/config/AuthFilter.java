package com.gatewayservice.config;

import com.gatewayservice.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;

@Component
public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {

    private WebClient.Builder webClientBuilder;

    @Autowired
    public AuthFilter(WebClient.Builder webClientBuilder) {
        super(Config.class);
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
                throw new RuntimeException("Missing authorization information");
            }

            String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
            System.err.println(authHeader);
            String[] parts = authHeader.split(" ");

            if (parts.length != 2 || !"Bearer".equals(parts[0])) {
                throw new RuntimeException("Incorrect authorization structure");
            }

//            return webClientBuilder.build()
//                    .post()
//                    .uri("http://AuthService/auth/validate-token?token=" + parts[1])
//                    .retrieve().bodyToMono(UserDto.class)
//                    .map(userDto -> {
//                        exchange.getRequest()
//                                .mutate()
//                                .header("X-auth-user-id", userDto.getId());
//                        System.err.println(exchange);
//                        return exchange;
//                    }).flatMap(chain::filter);
            return WebClient.create()
                    .post()
                    .uri("http://localhost:8000/auth/validate-token?token=" + parts[1])
                    .retrieve()
                    .bodyToMono(UserDto.class)
                    .map(userDto -> {
                        exchange.getRequest().mutate().header("X-auth-user-id", userDto.getId());
                        return exchange;
                    })
                    .flatMap(chain::filter);
        };
    }

    public static class Config {
        // empty class as I don't need any particular configuration
    }
}
