package com.productservice.model;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Data
@Document("products")
public class Product {
    @Id
    private String id;

    @NotBlank(message = "Product code is mandatory")
    @Indexed(unique = true)
    private String code;

    @NotBlank(message = "Product name is mandatory")
    private String name;

    @Range(min = 0, message = "Price must greater than or equal 0")
    private Double price;
}
