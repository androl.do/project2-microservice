package com.productservice.model;

import lombok.Data;

import java.util.Date;

@Data
public class ObjectResponse {
    private Date time = new Date();
    private String status;
    private String message;
    private Object data;

    public static String SUCCESS="SUCCESS";
    public static String FAILED="FAILED";

    public ObjectResponse(String status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
