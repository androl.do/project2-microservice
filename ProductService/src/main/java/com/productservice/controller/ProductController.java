package com.productservice.controller;
import com.productservice.exception.ProductException;
import com.productservice.model.ObjectResponse;
import com.productservice.model.Product;
import com.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

@RestController
@RequestMapping("/products")
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//@PreAuthorize("hasAuthority('ADMIN')")
public class ProductController {
    @Autowired
    private ProductService productService;

    /*Get all product*/
    @GetMapping()
    public ResponseEntity<ObjectResponse> getProductsByFilter(@RequestParam(value = "name",defaultValue = "") String name, @RequestParam("pageNumber") int pageNumber, @RequestParam("pageSize") int pageSize,@RequestParam(value = "sort",required = false) String sort){
        try{
            Pageable pageable;
            if (sort!=null && !sort.isEmpty()){
                String[] sortArr = sort.split(":");
                int sortDir = Integer.parseInt(sortArr[1]);
                pageable = PageRequest.of(pageNumber,pageSize, Sort.by(sortDir==1? Sort.Direction.ASC: Sort.Direction.DESC,sortArr[0]));

            }else{
                pageable = PageRequest.of(pageNumber,pageSize);
            }
            Page<Product> page = productService.getProductsByFilter(name,pageable);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get products by filter successfully",page));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }

    }

    /*Create new product*/
    @PostMapping
    public ResponseEntity<ObjectResponse> createProduct(@RequestBody Product product){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create product successfully",productService.createProduct(product)));
        }
        catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (ProductException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Update customer*/
    @PutMapping("/{id}")
    public ResponseEntity<ObjectResponse> updateProduct(@PathVariable("id") String id, @RequestBody Product product){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Update product successfully",productService.updateProduct(id,product)));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (ProductException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Delete data customer*/
    @DeleteMapping("/{id}")
    public ResponseEntity<ObjectResponse> deleteProduct(@PathVariable("id") String id){
        try {
            productService.deleteProduct(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Delete product with id: " +id+ " successfully",""));
        }catch (ProductException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    @GetMapping("/{code}")
    public ResponseEntity<ObjectResponse> getProductByCode(@PathVariable("code") String code){
        Optional<Product> productOpt = productService.getProductByCode(code);
        if (productOpt.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get product by code successfully",productOpt.get()));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found product with code: " + code,""));
        }
    }

}
