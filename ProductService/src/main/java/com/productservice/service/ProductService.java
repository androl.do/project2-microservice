package com.productservice.service;


import com.productservice.exception.ProductException;
import com.productservice.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> getAllProducts();

    Product createProduct(Product newProduct) throws ProductException;

    Product updateProduct(String id, Product newProduct) throws ProductException;

    void deleteProduct(String id) throws  ProductException;

    Page<Product> getProductsByFilter(String name, Pageable pageable) throws Exception;

    Optional<Product> getProductByCode(String code);
}
