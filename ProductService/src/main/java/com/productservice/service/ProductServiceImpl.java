package com.productservice.service;

import com.productservice.exception.ProductException;
import com.productservice.model.Product;
import com.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepo;


    @Override
    public List<Product> getAllProducts(){
        List<Product>  products = productRepo.findAll();
        if (!products.isEmpty()){
            return products;
        }else{
            return new ArrayList<>();
        }
    }


    @Override
    public Product createProduct(Product newProduct) throws ProductException, ConstraintViolationException {
        if (productRepo.findProductByCode(newProduct.getCode()).isPresent()){
            throw  new ProductException(ProductException.productCodeAlreadyExist());
        }else{
            return productRepo.save(newProduct);
        }
    }


    @Override
    public Product updateProduct(String id, Product newProduct) throws ProductException,ConstraintViolationException {
        Optional<Product> productOpt = productRepo.findById(id);
        if (!productOpt.isPresent()){
            throw new ProductException(ProductException.notFoundException(id));
        }else{
            Product oldProduct = productOpt.get();
            oldProduct.setName(newProduct.getName()==null ? oldProduct.getName() : newProduct.getName());
            oldProduct.setPrice(newProduct.getPrice()==null ? oldProduct.getPrice() : newProduct.getPrice());
            return productRepo.save(oldProduct);
        }
    }


    @Override
    public void deleteProduct(String id) throws  ProductException{
        Optional<Product> productOpt = productRepo.findById(id);
        if (!productOpt.isPresent()){
            throw new ProductException(ProductException.notFoundException(id));
        }else{
            productRepo.delete(productOpt.get());
        }
    }

    @Override
    public Page<Product> getProductsByFilter(String name, Pageable pageable) throws Exception {
        try {
            return productRepo.findProductsByNameContaining(name,pageable);
        }catch (Exception e){
            throw new Exception("Filter param invalid",e);
        }
    }

    @Override
    public Optional<Product> getProductByCode(String code){
        return productRepo.findProductByCode(code);
    }

}
