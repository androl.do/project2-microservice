package com.customerservice.service;

import com.customerservice.exception.CustomerException;
import com.customerservice.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer createCustomer(Customer newCus) throws CustomerException;

    Customer updateCustomer(String id, Customer cus) throws CustomerException;

    void deleteCustomer(String id) throws CustomerException;

    Page<Customer> getCustomersByFilter(String name, Pageable pageable) throws Exception;

    Optional<Customer> getCustomerById(String id);
}
