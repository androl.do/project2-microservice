package com.customerservice.service;

import com.customerservice.exception.CustomerException;
import com.customerservice.model.Customer;
import com.customerservice.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepo;

    @Override
    public List<Customer> getAllCustomers(){
        List<Customer>  customers = customerRepo.findAll();
        if (!customers.isEmpty()){
            return customers;
        }else{
            return new ArrayList<>();
        }
    }


    @Override
    public Customer createCustomer(Customer newCus) throws CustomerException, ConstraintViolationException {

        if (customerRepo.findCustomerByEmail(newCus.getEmail()).isPresent()) {
            throw new CustomerException(CustomerException.emailAlreadyExist());
        }
        if (customerRepo.findCustomerByPhone(newCus.getPhone()).isPresent()) {
            throw new CustomerException(CustomerException.phoneAlreadyExist());
        }
        return customerRepo.save(newCus);
    }

    @Override
    public Customer updateCustomer(String id, Customer cus) throws CustomerException, ConstraintViolationException {
        Optional<Customer> cusOpt = customerRepo.findById(id);
        if (!cusOpt.isPresent()){
            throw  new CustomerException(CustomerException.notFoundException(id));
        }
        Customer oldCus = cusOpt.get();
        if (cus.getEmail() != null) {
            if (customerRepo.findCustomerByEmail(cus.getEmail()).isPresent()) {
                throw new CustomerException(CustomerException.emailAlreadyExist());
            } else {
                oldCus.setEmail(cus.getEmail());
            }
        }
        if (cus.getPhone() != null) {
            if (customerRepo.findCustomerByPhone(cus.getPhone()).isPresent()) {
                throw new CustomerException(CustomerException.phoneAlreadyExist());
            } else {
                oldCus.setPhone(cus.getPhone());
            }
        }
        oldCus.setName(cus.getName()==null? oldCus.getName() : cus.getName());
        oldCus.setAddress(cus.getAddress()==null? oldCus.getAddress() : cus.getAddress());
        return customerRepo.save(oldCus);
    }

    @Override
    public void deleteCustomer(String id) throws CustomerException {
        Optional<Customer> cusOpt = customerRepo.findById(id);
        if (!cusOpt.isPresent()){
            throw new CustomerException(CustomerException.notFoundException(id));
        }else{
            customerRepo.delete(cusOpt.get());
        }
    }

    @Override
    public Page<Customer> getCustomersByFilter(String name, Pageable pageable) throws Exception {
        try {
            return customerRepo.findCustomersByNameContaining(name, pageable);
        }catch (Exception e){
            throw new Exception("Filter param invalid",e);
        }
    }

    @Override
    public Optional<Customer> getCustomerById(String id){
        return customerRepo.findCustomerById(id);
    }



}
