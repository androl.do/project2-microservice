package com.customerservice.repository;

import com.customerservice.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends MongoRepository<Customer,String> {
    Optional<Customer> findCustomerByPhone(String phone);
    Optional<Customer> findCustomerByEmail(String phone);
    Page<Customer> findCustomersByNameContaining(String name,Pageable pageable);
    Optional<Customer> findCustomerById(String id);
}
