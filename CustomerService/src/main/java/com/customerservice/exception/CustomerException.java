package com.customerservice.exception;

public class CustomerException extends  Exception{

    public CustomerException(String message) {
        super(message);
    }

    public  static String notFoundException(String id){
        return "Customer with id: " + id + " not found";
    }

    public  static String emailAlreadyExist(){
        return "This email already exist";
    }

    public  static String phoneAlreadyExist(){
        return "This phone already exist";
    }

}
