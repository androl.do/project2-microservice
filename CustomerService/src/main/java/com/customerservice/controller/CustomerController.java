package com.customerservice.controller;

import com.customerservice.exception.CustomerException;
import com.customerservice.model.Customer;
import com.customerservice.model.ObjectResponse;
import com.customerservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /*Get all customer*/
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    @GetMapping()
    public ResponseEntity<ObjectResponse> getCustomersByFilter(@RequestParam(value = "name",defaultValue = "") String name, @RequestParam("pageNumber") int pageNumber, @RequestParam("pageSize") int pageSize,@RequestParam(value = "sort",required = false) String sort){
        try{
            Pageable pageable;
            if (sort!=null && !sort.isEmpty()){
                String[] sortArr = sort.split(":");
                int sortDir = Integer.parseInt(sortArr[1]);
                pageable = PageRequest.of(pageNumber,pageSize,Sort.by(sortDir==1? Sort.Direction.ASC: Sort.Direction.DESC,sortArr[0]));

            }else{
                pageable = PageRequest.of(pageNumber,pageSize);
            }
            Page<Customer> page = customerService.getCustomersByFilter(name,pageable);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get users by filter successfully",page));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }

    }

    /*Create new customer*/
    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public ResponseEntity<ObjectResponse> createCustomer(@RequestBody Customer customer, Principal principal){
        try {
            customer.setUserName(principal.getName());
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create customers successfully",customerService.createCustomer(customer)));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
        catch (CustomerException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Update customer*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ObjectResponse> updateCustomer(@PathVariable("id") String id, @RequestBody Customer customer){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse("SUCCESS","Create customers successfully",customerService.updateCustomer(id,customer)));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
        catch (CustomerException e){
            return ResponseEntity.status(e.equals(CustomerException.notFoundException(id))?HttpStatus.NOT_FOUND : HttpStatus.CONFLICT)
                    .body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    /*Delete data customer*/
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ObjectResponse> deleteCustomer(@PathVariable("id") String id){
        try {
            customerService.deleteCustomer(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Delete customer with id: " +id+ " successfully",""));
        }catch (CustomerException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ObjectResponse> getCustomerById(@PathVariable("id") String id){
        Optional<Customer> customerOpt = customerService.getCustomerById(id);
        if (customerOpt.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get customer by code successfully",customerOpt.get()));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found customer with id: " + id,""));
        }
    }

}
