package com.customerservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Document("customers")
public class Customer {
    @Id
    private String id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @Indexed(unique = true)
    @Pattern(regexp = "^(0|\\+84)(\\s|\\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\\d)(\\s|\\.)?(\\d{3})(\\s|\\.)?(\\d{3})$",message = "Phone invalid")
    private String phone;

    @Indexed(unique = true)
    @Email(message = "Email invalid")
    private String email;

    @NotBlank(message = "User name is mandatory")
    private String userName;

    @NotNull(message = "Address can't be null")
    private Address address;
}
