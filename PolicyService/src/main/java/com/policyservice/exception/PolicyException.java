package com.policyservice.exception;

public class PolicyException extends  Exception{

    public PolicyException(String message) {
        super(message);
    }

    public  static String notFoundException(String id){
        return "Permission with id: " + id + " not found";
    }

    public  static String policyCodeAlreadyExist(){
        return "This policy code already exist";
    }

}
