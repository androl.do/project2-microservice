package com.policyservice.repository;

import com.policyservice.model.Policy;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PolicyRepository extends MongoRepository<Policy, String> {

    Optional<Policy> findByCode(String code);
}
