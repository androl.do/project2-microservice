package com.policyservice.repository;
import com.policyservice.model.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;
import request.ObjectRequest;

@Repository
public class PolicyCustomRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public Page<Policy> findUsersByProperties(ObjectRequest objectRequest) throws Exception {
        try {
            return PageableExecutionUtils.getPage(mongoTemplate.find(objectRequest.toQueryMongo(), Policy.class), objectRequest.getPageable(),
                    ()-> mongoTemplate.count(objectRequest.toQueryMongo(), Policy.class));
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Search param invalid",e);
        }
    }
}
