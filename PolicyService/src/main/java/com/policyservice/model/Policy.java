package com.policyservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Document("policies")
public class Policy {
    @Id
    private String id;

    @NotBlank(message = "Policy code is mandatory")
    private String code;

    @NotBlank(message = "Quote code is mandatory")
    private String quoteCode;

    @NotBlank(message = "Username is mandatory")
    private String userName;

    @NotBlank(message = "Customer id is mandatory")
    private String customerId;

    @NotBlank(message = "Product code is mandatory")
    private String productCode;

    @NotNull(message = "Price code is mandatory")
    @Range(min = 0, message = "Price must greater than or equal 0")
    private Double price;

    @NotNull(message = "Effective date is mandatory")
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date effectiveDate;

    @NotNull(message = "Expiration date is mandatory")
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date expirationDate;
}
