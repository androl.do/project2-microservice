package com.policyservice.service;

import com.policyservice.exception.PolicyException;
import com.policyservice.model.Policy;
import com.policyservice.repository.PolicyCustomRepository;
import com.policyservice.repository.PolicyRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import request.ObjectRequest;
import request.Search;
import response.ObjectResponse;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class PolicyServiceImpl implements  PolicyService{
    @Autowired
    private PolicyRepository policyRepo;
    @Autowired
    private PolicyCustomRepository policyCustomRepo;
    @Autowired
    private RestTemplate restTemplate;
    private final String urlQuote = "http://localhost:8004/quotes";

    @Transactional
    @Override
    public Policy createPolicy(Policy policy, Principal principal) throws PolicyException {

        if (policyRepo.findByCode(policy.getCode()).isPresent()){
            throw  new PolicyException(PolicyException.policyCodeAlreadyExist());
        }

        String username = null;
        String customerId= "";
        String productCode = "";
        try {
            ObjectResponse responseQuote = restTemplate.getForObject(urlQuote + "/" + policy.getQuoteCode(), ObjectResponse.class);
            if (responseQuote!=null){
                HashMap<String,String> mapQuote = (HashMap<String, String>) responseQuote.getData();
                username = mapQuote.get("userName");
                customerId = mapQuote.get("customerId");
                productCode = mapQuote.get("productCode");
            }
        }catch (Exception e){
            throw new PolicyException(e.getMessage());
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean admin = authentication.getAuthorities().stream().anyMatch(au-> au.getAuthority().equalsIgnoreCase("ADMIN"));
        if (!admin && !StringUtils.isBlank(username)  && !username.equalsIgnoreCase(principal.getName())){
            throw new PolicyException("User not permission release this policy");
        }
        policy.setUserName(principal.getName());
        policy.setCustomerId(customerId);
        policy.setProductCode(productCode);

        if (policy.getEffectiveDate().before(new Date())){
            throw new PolicyException("Effective date must before now date");
        }

        if (policy.getEffectiveDate().after(policy.getExpirationDate())){
            throw new PolicyException("Expiration date must after effective date");
        }

        return policyRepo.save(policy);
    }

    @Override
    @Transactional
    public Page<Policy> getPolicyByFilter(ObjectRequest objectRequest,Principal principal) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean admin = authentication.getAuthorities().stream().anyMatch(au-> au.getAuthority().equalsIgnoreCase("ADMIN"));
        if (!admin){
            List<Search> searches = objectRequest.getSearches();
            if (searches == null)
                searches = new ArrayList<>();
            searches.add(new Search("userName", principal.getName()));
            objectRequest.setSearches(searches);
        }

        return policyCustomRepo.findUsersByProperties(objectRequest);
    }


}
