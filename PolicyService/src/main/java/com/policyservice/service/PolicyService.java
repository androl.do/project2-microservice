package com.policyservice.service;

import com.policyservice.exception.PolicyException;
import com.policyservice.model.Policy;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import request.ObjectRequest;

import java.security.Principal;

public interface PolicyService {
    Policy createPolicy(Policy policy, Principal principal) throws PolicyException;

    @Transactional
    Page<Policy> getPolicyByFilter(ObjectRequest objectRequest,Principal principal) throws Exception;
}
