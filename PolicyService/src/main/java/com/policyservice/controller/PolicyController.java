package com.policyservice.controller;
import com.policyservice.exception.PolicyException;
import com.policyservice.model.Policy;
import com.policyservice.service.PolicyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import request.ObjectRequest;
import response.ObjectResponse;

import javax.validation.ConstraintViolationException;
import java.security.Principal;

@RestController
@RequestMapping("/policies")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
public class PolicyController {

    @Autowired
    private PolicyService policyService;

    @PostMapping("/filter")
    public ResponseEntity<ObjectResponse> getPolicyByFilter(@RequestBody ObjectRequest objectRequest,Principal principal){
        try{
            Page<Policy> page = policyService.getPolicyByFilter(objectRequest,principal);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get policy successfully",page));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    @PostMapping
    public ResponseEntity<ObjectResponse> createPolicy(@RequestBody Policy policy, Principal principal){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create policy successfully",policyService.createPolicy(policy,principal)));
        }
        catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (PolicyException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }



}
