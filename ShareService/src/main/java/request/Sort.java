package request;

import lombok.Data;

@Data
public class Sort {
    private String name;
    private int value;
}
