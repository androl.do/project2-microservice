package request;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class ObjectRequest {
    private List<Search> searches;
    private Sort sort;
    private Page page;

    public  Pageable getPageable(){
        return PageRequest.of(this.page.getPageNumber(),this.page.getPageSize());
    }

    public Query toQueryMongo(){
        Query query = new Query();
        final List<Criteria> criteria = new ArrayList<>();

        if (!CollectionUtils.isEmpty(searches)){
            searches.forEach(s->
                criteria.add(Criteria.where(s.getName()).is(s.getValue()))
            );
        }

        if (!criteria.isEmpty()){
            query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
        }

        if (this.sort!=null){
            query.with(org.springframework.data.domain.Sort.by(this.sort.getValue()==1? org.springframework.data.domain.Sort.Direction.ASC: org.springframework.data.domain.Sort.Direction.DESC,this.sort.getName()));
        }

        query.with(getPageable());

        return query;

    }
}
