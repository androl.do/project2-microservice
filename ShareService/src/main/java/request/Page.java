package request;

import lombok.Data;

@Data
public class Page {
    private int pageNumber;
    private int pageSize;
}
