package com.quoteservice.exception;

public class QuoteException extends  Exception{

    public QuoteException(String message) {
        super(message);
    }

    public  static String notFoundProductException(String code){
        return "Product with code: " + code + " not found";
    }

    public  static String notFoundCustomerException(String id){
        return "Customer with id: " + id + " not found";
    }


    public  static String codeAlreadyExist(){
        return "This code already exist";
    }


}
