package com.quoteservice.service;

import com.quoteservice.exception.QuoteException;
import com.quoteservice.model.ObjectResponse;
import com.quoteservice.model.Quote;
import com.quoteservice.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

@Service
public class QuoteServiceImpl implements  QuoteService{
    @Autowired
    private QuoteRepository quoteRepository;
    @Autowired
    private RestTemplate restTemplate;
    private final String urlProduct = "http://localhost:8003/products";
    private final String urlCustomer = "http://localhost:8002/customers";

    @Override
    public Quote createQuote(Quote quote) throws QuoteException, ConstraintViolationException {

        if (quoteRepository.findQuoteByCode(quote.getCode()).isPresent()){
            throw new QuoteException(QuoteException.codeAlreadyExist());
        }

        try {
            restTemplate.getForObject(urlProduct + "/" + quote.getProductCode(), ObjectResponse.class);
        }catch (Exception e){
            throw new QuoteException(QuoteException.notFoundProductException(quote.getProductCode()));
        }

        String userName = null;
        try {
            ObjectResponse responseCustomer = restTemplate.getForObject(urlCustomer + "/" + quote.getCustomerId(), ObjectResponse.class);
            HashMap<String,String> mapCus = (HashMap<String, String>) responseCustomer.getData();
            userName =mapCus.get("userName");
        }catch (Exception e){
            e.printStackTrace();
            throw new QuoteException(QuoteException.notFoundCustomerException(quote.getCustomerId()));
        }


        if (userName==null || !userName.equalsIgnoreCase(quote.getUserName())){
            throw new QuoteException("User not permission create quote for this customer");
        }

        if (quote.getEffectiveDate().before(new Date())){
            throw new QuoteException("Effective date must before now date");
        }

        if (quote.getEffectiveDate().after(quote.getExpirationDate())){
            throw new QuoteException("Expiration date must after effective date");
        }

        return quoteRepository.save(quote);
    }

    @Override
    public Page<Quote> getAllQuote(Pageable pageable) throws Exception {
        try{
            return quoteRepository.findAll(pageable);
        }catch (Exception e){
            throw new Exception("Filter param invalid");
        }
    }

    @Override
    public Optional<Quote> getQuoteByCode(String code){
        return quoteRepository.findQuoteByCode(code);
    }
}
