package com.quoteservice.service;

import com.quoteservice.exception.QuoteException;
import com.quoteservice.model.Quote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface QuoteService {
    Quote createQuote(Quote quote) throws QuoteException;

    Page<Quote> getAllQuote(Pageable pageable) throws Exception;

    Optional<Quote> getQuoteByCode(String code);
}
