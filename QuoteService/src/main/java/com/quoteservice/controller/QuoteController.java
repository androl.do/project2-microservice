package com.quoteservice.controller;

import com.quoteservice.exception.QuoteException;
import com.quoteservice.model.ObjectResponse;
import com.quoteservice.model.Quote;
import com.quoteservice.service.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/quotes")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class QuoteController {
    @Autowired
    private QuoteService quoteService;

    @GetMapping()
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public ResponseEntity<ObjectResponse> getQuotesByFilter(@RequestParam("pageNumber") int pageNumber, @RequestParam("pageSize") int pageSize,@RequestParam(value = "sort",required = false) String sort){
        try{
            Pageable pageable;
            if (sort!=null && !sort.isEmpty()){
                String[] sortArr = sort.split(":");
                int sortDir = Integer.parseInt(sortArr[1]);
                pageable = PageRequest.of(pageNumber,pageSize, Sort.by(sortDir==1? Sort.Direction.ASC: Sort.Direction.DESC,sortArr[0]));

            }else{
                pageable = PageRequest.of(pageNumber,pageSize);
            }
            Page<Quote> page = quoteService.getAllQuote(pageable);
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get quotes by filter successfully",page));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }

    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    public ResponseEntity<ObjectResponse> createQuote(@RequestBody Quote quote, Principal principal){
        try {
            quote.setUserName(principal.getName());
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Create user successfully",quoteService.createQuote(quote)));
        }catch (ConstraintViolationException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }catch (QuoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    @GetMapping("/{code}")
    public ResponseEntity<ObjectResponse> getQuoteByCode(@PathVariable("code") String code){
        Optional<Quote> quoteOpt = quoteService.getQuoteByCode(code);
        if (quoteOpt.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Get quote by code successfully",quoteOpt.get()));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ObjectResponse(ObjectResponse.FAILED,"Not found quote with code: " + code,""));
        }
    }

}
