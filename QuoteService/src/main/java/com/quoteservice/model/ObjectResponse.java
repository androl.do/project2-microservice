package com.quoteservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
public class ObjectResponse {
    private Date time = new Date();
    private String status;
    private String message;
    private Object data;

    public static String SUCCESS="SUCCESS";
    public static String FAILED="FAILED";

    public ObjectResponse(String status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ObjectResponse(Date time, String status, String message, Object data) {
        this.time = time;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ObjectResponse() {
    }
}
