package com.quoteservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Document("quotes")
public class Quote {
    @Id
    private String id;

    @NotBlank(message = "Quote code is mandatory")
    private String code;

    @Range(min = 0,message = "Price must greater than or equal 0")
    private double price;

    @NotNull(message = "Effective date is mandatory")
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date effectiveDate;

    @NotNull(message = "Expiration date is mandatory")
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date expirationDate;

    @NotBlank(message = "Product code is mandatory")
    private String productCode;

    @NotBlank(message = "Customer id code is mandatory")
    private String customerId;

    @NotBlank(message = "User name id is mandatory")
    private String userName;
}
