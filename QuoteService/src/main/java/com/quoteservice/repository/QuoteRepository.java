package com.quoteservice.repository;

import com.quoteservice.model.Quote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface QuoteRepository extends MongoRepository<Quote,String> {
    Optional<Quote> findQuoteByCode(String code);
    Page<Quote> findAll(Pageable pageable);
}
