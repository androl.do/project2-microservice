package com.authservice.dto;

import com.authservice.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserDto {

    private String id;
    private String login;
    private String token;

    public static UserDto toUserDto(User user,String token){
        UserDto userDto = new UserDto(user.getId(), user.getUsername(), token);
        return userDto;
    }
}
