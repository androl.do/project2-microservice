package com.authservice.controller;

import com.authservice.dto.CredentialsDto;
import com.authservice.dto.UserDto;
import com.authservice.model.ObjectResponse;
import com.authservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@Slf4j
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/sign-in")
    public ResponseEntity<ObjectResponse> signIn(@RequestBody CredentialsDto credentialsDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new ObjectResponse(ObjectResponse.SUCCESS,"Sign in success",userService.signIn(credentialsDto)));
        } catch (Exception e) {
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ObjectResponse(ObjectResponse.FAILED,e.getMessage(),""));
        }
    }

    @PostMapping("/validate-token")
    public ResponseEntity<UserDto> signIn(@RequestParam String token) {
        log.info("Trying to validate token {}", token);
        try {
            return  ResponseEntity.ok(userService.validateToken(token));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new UserDto());
        }
    }
}
