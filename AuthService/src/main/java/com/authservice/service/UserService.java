package com.authservice.service;

import com.authservice.dto.CredentialsDto;
import com.authservice.dto.UserDto;

public interface UserService {
    UserDto signIn(CredentialsDto credentialsDto) throws Exception;

    UserDto validateToken(String token) throws Exception;
}
