package com.authservice.model;

import lombok.Data;

@Data
public class Address {
    private String city;
    private String district;
    private String ward;
}
