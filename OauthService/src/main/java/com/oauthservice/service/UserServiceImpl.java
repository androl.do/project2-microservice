package com.oauthservice.service;

import com.oauthservice.model.Rule;
import com.oauthservice.model.User;
import com.oauthservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findUserByUsername(username);
        System.err.println("========================"+user);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),mapRules(user.getRules()));
    }

    private List<? extends GrantedAuthority> mapRules(List<Rule> rules){
        return rules.stream().map(rule -> new SimpleGrantedAuthority(rule.getName())).collect(Collectors.toList());
    }

}
